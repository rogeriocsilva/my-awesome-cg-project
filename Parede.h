#include "Alvo.h"

class Parede
{
public:
	Parede(){
	}
	
	~Parede(){}

	Parede(Coord a, Coord b, GLfloat c){
		start_point = a;
		end_point = b;
		height = c;
	}

	void setStartPoint(Coord);
	void setEndPoint(Coord);
	void setHeight(GLfloat);

	GLfloat getHeight();
	Coord getStartPoint();
	Coord getEndPoint();
private:
	Coord start_point;
	Coord end_point;
	GLfloat height;
};