#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include "Camera.h"

void Camera::Init()
{
	m_yaw = 0.0;
	m_pitch = 0.0;
	Coord xyz;
	xyz.x= 3;
	xyz.y=1;
	xyz.z=7;
	SetPos(xyz);
}

void Camera::Refresh()
{
	// Camera parameter according to Riegl's co-ordinate system
	// x/y for flat, z for height
	m_lx = cos(m_yaw) * cos(m_pitch);
	m_ly = sin(m_pitch);
	m_lz = sin(m_yaw) * cos(m_pitch);

	m_strafe_lx = cos(m_yaw - M_PI_2);
	m_strafe_lz = sin(m_yaw - M_PI_2);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(m_x, m_y, m_z, m_x + m_lx, m_y + m_ly, m_z + m_lz, 0.0,1.0,0.0);

	//printf("Camera: %f %f %f Direction vector: %f %f %f\n", m_x, m_y, m_z, m_lx, m_ly, m_lz);
}

void Camera::SetPos(Coord coordenadas)
{
	m_x = coordenadas.x;
	m_y = coordenadas.y;
	m_z = coordenadas.z;

	Refresh();
}

Coord Camera::GetPos(void)
{	
	Coord coordenadas;
    coordenadas.x = m_x;
    coordenadas.y = m_y;
    coordenadas.z = m_z;
    return coordenadas;
}


float Camera::GetYaw()
{
    return m_yaw;
}

float Camera::GetPitch()
{
    return m_pitch;
}

Coord Camera::GetDirectionVector()
{
	Coord vector;
    vector.x = m_lx;
    vector.y = m_ly;
    vector.z = m_lz;
    return vector;
}

void Camera::Move(float incr)
{
    float lx = cos(m_yaw)*cos(m_pitch);
    //float ly = sin(m_pitch);
    float lz = sin(m_yaw)*cos(m_pitch);

	m_x = m_x + incr*lx;
	//m_y = m_y + incr*ly;
	m_z = m_z + incr*lz;

	Refresh();
}

void Camera::Strafe(float incr)
{
	m_x = m_x + incr*m_strafe_lx;
	m_z = m_z + incr*m_strafe_lz;

	Refresh();
}


void Camera::RotateYaw(float angle)
{
	m_yaw += angle;

	Refresh();
}

void Camera::RotatePitch(float angle)
{
    const float limit = 89.0 * M_PI / 180.0;

	m_pitch += angle;

    if(m_pitch < -limit)
        m_pitch = -limit;

    if(m_pitch > limit)
        m_pitch = limit;

	Refresh();
}

void Camera::SetYaw(float angle)
{
	m_yaw = angle;

	Refresh();
}

void Camera::SetPitch(float angle)
{
    m_pitch = angle;

    Refresh();
}
