#include "Alvo.h"



void Alvo::Init( )
{
	glPushMatrix();
        glNormal3f((GLfloat)direcao.x,(GLfloat)direcao.y,(GLfloat)direcao.z);
        GLUquadricObj *quadratic;
        quadratic = gluNewQuadric();
        glColor4f(VERMELHO);
        gluDisk(quadratic, 0,0.005, 30, 1);
        int j=0;
        for(float i=0; i < 0.5; i+=0.05) {
            if(j<2)
                glColor4f(AMARELO);
                
            else if(j<4)
                glColor4f(VERMELHO);
                
            else if(j<6)
                glColor4f(AZUL);
                
            else if(j<8)
                glColor4f(BLACK);
                
            else
                glColor4f(WHITE);

            gluDisk(quadratic, i+0.005, i+0.05, 30, 1);
            glColor4f(BLACK);
            gluDisk(quadratic, i+0.05, i+0.055, 30, 1);
            j++;
        }

    glPopMatrix();
}



void Alvo::setDirecao(Coord a){
    direcao = a;
}

void Alvo::setPos(Coord a){
    pos = a;
}
Coord Alvo::getDirecao(){
    return direcao;
}
Coord Alvo::getPos(){
    return pos;
}