#ifndef __ALVO_H__
#define __ALVO_H__

#include "header.h"

class Alvo{
	public:
		Alvo(){}

		void Init();
		void Boneco();
		void setDirecao(Coord a);
		void setPos(Coord a);

		Coord getPos();
		Coord getDirecao();

	private:
		Coord pos;
		Coord direcao;

};
#endif