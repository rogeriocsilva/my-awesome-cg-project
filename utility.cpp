#include "header.h"

GLfloat norma (Coord vetor){
	return( (GLfloat)sqrt( vetor.x * vetor.x + vetor.y * vetor.y + vetor.z * vetor.z ) );
}

GLfloat produto_interno(Coord v1, Coord v2){
	return( (GLfloat)(v1.x*v2.x + v1.y*v2.y + v1.z*v2.z) );
}

GLfloat angle(Coord v1, Coord v2){
	GLfloat aux = produto_interno(v1,v2) / (norma(v1)*norma(v2));
	aux = (GLfloat)acos( aux );
	if(v2.y>=0){
		return aux;
	}
	else{
		return -aux;
	}
}

Coord crossProduct(Coord v1, Coord v2){
	Coord new_vec;
	new_vec.x = v1.y*v2.z - v1.z*v2.y;
	new_vec.y = v1.z*v2.x - v1.x*v2.z;
	new_vec.z = v1.x*v2.y - v1.y*v2.x;
	return new_vec;
}

GLfloat distance(Coord v1, Coord v2){
	return((GLfloat)sqrt((v2.x-v1.x)*(v2.x-v1.x)+(v2.y-v1.y)*(v2.y-v1.y)+(v2.z-v1.z)*(v2.z-v1.z)));
}