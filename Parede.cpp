#include "Parede.h"


void Parede::setStartPoint(Coord a){
	start_point = a;
}

void Parede::setHeight(GLfloat a){
	height = a;
}

void Parede::setEndPoint(Coord a){
	end_point = a;
}

GLfloat Parede::getHeight(){
	return height;
}

Coord Parede::getStartPoint(){
	return start_point;
}
Coord Parede::getEndPoint(){
	return end_point;
}
