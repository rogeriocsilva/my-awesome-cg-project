#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Parede.h"

class Camera
{
public:
	Camera() { Init(); }
	~Camera(){}
	
	void Init();
	void Refresh();
	void SetPos(Coord);
    Coord GetPos();
    GLfloat GetYaw();
    GLfloat GetPitch();
    Coord GetDirectionVector();
	void SetYaw(GLfloat angle);
	void SetPitch(GLfloat angle);

	// Navigation
	void Move(GLfloat incr);
	void Strafe(GLfloat incr);
	void Fly(GLfloat incr);
	void RotateYaw(GLfloat angle);
	void RotatePitch(GLfloat angle);

private:
	GLfloat m_x, m_y, m_z;   // Position
    GLfloat m_lx, m_ly, m_lz; // Direction vector of where we are looking at
	GLfloat m_yaw, m_pitch; // Various rotation angles
	GLfloat m_strafe_lx, m_strafe_lz; // Always 90 degree to direction vector
};

#endif
