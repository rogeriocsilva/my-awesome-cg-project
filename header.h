#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "RgbImage.h"
#include <vector>
#include <time.h>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif


#define AZUL     0.0, 0.0, 1.0, 1.0
#define VERMELHO 1.0, 0.0, 0.0, 1.0
#define AMARELO  1.0, 1.0, 0.0, 1.0
#define VERDE    0.0, 1.0, 0.0, 1.0
#define LARANJA  0.8, 0.6, 0.1, 1.0
#define WHITE    1.0, 1.0, 1.0, 1.0
#define BLACK    0.0, 0.0, 0.0, 1.0
#define GRAY1    0.2, 0.2, 0.2, 1.0
#define GRAY2    0.93, 0.93, 0.93, 1.0

#define WALL_HEIGHT 3.0
#define MAX_ARROW_ON_SCREEN 5000
#define MAX_VELO_ARROW 5

using namespace std;



#include <math.h>

typedef struct _Coord
{
    GLfloat x;
    GLfloat y;
    GLfloat z;
}Coord;


typedef struct _Arrow{
	Coord pos;
	Coord vectores;
	GLfloat time;
	GLfloat angle;
	GLfloat speed;
	Coord cor1,cor2,cor3;
}Arrow;


GLfloat norma (Coord);
GLfloat produto_interno(Coord , Coord );
GLfloat angle(Coord v1, Coord v2);

void initLights(void);
void criaDefineTexturas();
void init(void);
void onClickDown(void);
void drawArrows(void);
void shootArrow(void);
void display(void);
void Reshape (int w, int h);
void moveCamera(void);
void Update(int value);
void teclado (unsigned char key, int x, int y);
void Mouse(int button, int state, int x, int y);
void KeyboardUp(unsigned char key, int x, int y);
void MouseMotion(int x, int y);
void drawScene();
void CheckArrowColisions();
void drawArrow(Coord cor1,Coord cor2,Coord cor3);
Coord crossProduct(Coord v1, Coord v2);
GLfloat distance(Coord v1, Coord v2);
bool CheckArrowWallColisions(vector<Arrow>::iterator v);
void drawStaticArrows();
void drawGlass();
void drawWalls();
void drawAlvos();
void drawCandieiros();
void drawSkybox();
bool CheckAlvoArrowColision(vector<Arrow>::iterator v);
void moveAlvo();
