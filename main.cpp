#include "Camera.h"


//------------------------------------------------------------ Sistema Coordenadas
GLint g_viewport_width = 1280;
GLint g_viewport_height = 720;
//------------------------------------------------------------ Observador



Camera g_camera;
bool g_key[256];

// Movement settings
float g_translation_speed = 0.05;
float g_rotation_speed = M_PI/180*0.1;

//FORCE TEC9 ARMOR da seta
float force;

bool g_mouse_left_down = false;
bool g_mouse_right_down = false;




//------------------------------------------------------------ Iluminacao
bool light=true,lanterna=false,fog=false, inside = false , goingUp = true;





//------------------------------------------------------------ Texturas
GLuint  texture[20];
GLuint  tex;
RgbImage imag;

//------------------------------------------------------------ NOVO - Nevoeiro
GLfloat nevoeiroCor[] = {0.75, 0.75, 0.75, 1.0}; //definicao da cor do nevoeiro



//--------------------------------- cenas

GLfloat testeX=0,testeZ=0;
GLint pontuacao_jogador = 0;
GLint setas_disparadas = 0;

GLint tamanho;
GLint map[1000][1000];

char     texto[200];



vector<Arrow> arrow_array;
vector<Arrow> static_arrow_array;
vector<Parede> parede_array;
vector<Alvo> alvo_array;


GLfloat spotlightPosition[] = {0.0,3.0,22,1.0}; //spotlight position
GLfloat spotlightDirection[] = {0.0,-1.0,0.0}; //spotlight position
GLfloat spotlightColor[] = {1,0,0,1};//spot light color

GLfloat spotlightPosition2[] = {0.0,3.0,37.0,1.0}; //spotlight position
GLfloat spotlightDirection2[] = {0.0,-1.0,0.0}; //spotlight position
GLfloat spotlightColor2[] = {0.0,1.0,0.0,1.0};//spot light color

GLfloat spotlightPosition3[] = {14.0,3.0,22.0,1.0}; //spotlight position
GLfloat spotlightDirection3[] = {0.0,-1.0,0.0}; //spotlight position
GLfloat spotlightColor3[] = {0.0,0.0,1.0,1.0};//spot light color

GLfloat spotlightPosition4[] = {14.0,3.0,37.0,1.0}; //spotlight position
GLfloat spotlightDirection4[] = {0.0,-1.0,0.0}; //spotlight position
GLfloat spotlightColor4[] = {1.0,1.0,0.0,1.0};//spot light color

GLfloat lanternaPosition[] = {0.0,0.0,0.0,1.0}; //spotlight position
GLfloat lanternaDirection[] = {1.0,0.0,0.0}; //spotlight position
GLfloat lanternaColor[] = {1.0,1.0,1.0,1.0};//spot light color


GLfloat difuselightPosition[] = {33.0,3.0,35.0,0.0}; //spotlight position
GLfloat difuselightDirection[] = {0.0,-1.0,0.0}; //spotlight position
GLfloat difuselightColor[] = {1.0,1.0,1.0,1.0};//spot light color

//================================================================================
//=========================================================================== INIT
//================================================================================



//……………………………………………………………………………………………………………………………………………………… Iluminacao

void initLights(void){


    GLfloat ambiente[] = {1,1,1,1};




    GLfloat Foco_ak         = 1.0;
    GLfloat Foco_al         = 0.05f;
    GLfloat Foco_aq         = 0.0f;
    GLfloat Foco_Expon      = 2.0f;
    GLfloat angulo          = 70.0f;
        
    if(inside || !light){
        for (int i = 0; i < 4; ++i)
        {
            ambiente[i] = 0.3;
        }
    }

    //=================================================================Ambiente
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambiente);


    //=================================================================Foco
    glLightfv(GL_LIGHT0,GL_POSITION,                spotlightPosition);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,                 spotlightColor);
    glLightf(GL_LIGHT0,GL_CONSTANT_ATTENUATION,     Foco_ak);
    glLightf(GL_LIGHT0,GL_LINEAR_ATTENUATION,       Foco_al);
    glLightf(GL_LIGHT0,GL_QUADRATIC_ATTENUATION,    Foco_aq);
    glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,              angulo);
    glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,          spotlightDirection);
    glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,            Foco_Expon);

    glLightfv(GL_LIGHT1,GL_POSITION,                spotlightPosition2);
    glLightfv(GL_LIGHT1,GL_DIFFUSE,                 spotlightColor2);
    glLightf(GL_LIGHT1,GL_CONSTANT_ATTENUATION,     Foco_ak);
    glLightf(GL_LIGHT1,GL_LINEAR_ATTENUATION,       Foco_al);
    glLightf(GL_LIGHT1,GL_QUADRATIC_ATTENUATION,    Foco_aq);
    glLightf(GL_LIGHT1,GL_SPOT_CUTOFF,              angulo);
    glLightfv(GL_LIGHT1,GL_SPOT_DIRECTION,          spotlightDirection2);
    glLightf(GL_LIGHT1,GL_SPOT_EXPONENT,            Foco_Expon);

    glLightfv(GL_LIGHT2,GL_POSITION,                spotlightPosition3);
    glLightfv(GL_LIGHT2,GL_DIFFUSE,                 spotlightColor3);
    glLightf(GL_LIGHT2,GL_CONSTANT_ATTENUATION,     Foco_ak);
    glLightf(GL_LIGHT2,GL_LINEAR_ATTENUATION,       Foco_al);
    glLightf(GL_LIGHT2,GL_QUADRATIC_ATTENUATION,    Foco_aq);
    glLightf(GL_LIGHT2,GL_SPOT_CUTOFF,              angulo);
    glLightfv(GL_LIGHT2,GL_SPOT_DIRECTION,          spotlightDirection3);
    glLightf(GL_LIGHT2,GL_SPOT_EXPONENT,            Foco_Expon);

    glLightfv(GL_LIGHT3,GL_POSITION,                spotlightPosition4);
    glLightfv(GL_LIGHT3,GL_DIFFUSE,                 spotlightColor4);
    glLightf(GL_LIGHT3,GL_CONSTANT_ATTENUATION,     Foco_ak);
    glLightf(GL_LIGHT3,GL_LINEAR_ATTENUATION,       Foco_al);
    glLightf(GL_LIGHT3,GL_QUADRATIC_ATTENUATION,    Foco_aq);
    glLightf(GL_LIGHT3,GL_SPOT_CUTOFF,              angulo);
    glLightfv(GL_LIGHT3,GL_SPOT_DIRECTION,          spotlightDirection4);
    glLightf(GL_LIGHT3,GL_SPOT_EXPONENT,            Foco_Expon);


    glLightfv(GL_LIGHT5,GL_POSITION,                difuselightPosition);
    glLightfv(GL_LIGHT5,GL_DIFFUSE,                 difuselightColor);
    glLightf(GL_LIGHT5,GL_CONSTANT_ATTENUATION,     Foco_ak);
    glLightf(GL_LIGHT5,GL_LINEAR_ATTENUATION,       Foco_al);
    glLightf(GL_LIGHT5,GL_QUADRATIC_ATTENUATION,    Foco_aq);
    glLightf(GL_LIGHT5,GL_SPOT_CUTOFF,              angulo);
    glLightfv(GL_LIGHT5,GL_SPOT_DIRECTION,          difuselightDirection);
    glLightf(GL_LIGHT5,GL_SPOT_EXPONENT,            Foco_Expon);




    


}


void initNevoeiro(void){
    glFogfv(GL_FOG_COLOR, nevoeiroCor); //Cor do nevoeiro
    glFogi(GL_FOG_MODE, GL_EXP); //Equa‹o do nevoeiro - linear
    glFogf(GL_FOG_START, 1.0); // Dist‰ncia a que ter‡ in’cio o nevoeiro
    glFogf(GL_FOG_END, 2.5); // Dist‰ncia a que o nevoeiro terminar‡
    glFogf (GL_FOG_DENSITY, 0.10);
}



void initMap(void){
    FILE *fp;
    char buff[255];
    GLint k=0,l=0;
    fp = fopen("map.txt", "r");
    
    tamanho=100;
    while(fgets(buff,255, (FILE*)fp)!=NULL){
        for(int j=0;j<tamanho*2;j+=2){
            map[k][l] =buff[j]-48;
            l++;
        }
        l=0;
        k++;
    }
    fclose(fp);


    bool inWall = false;
    for(int i=0;i<tamanho;i++){
        for(int j=0;j<tamanho;j++){
            if(map[i][j]==1 && map[i][j+1]==1 ){
                if(!inWall){
                    Parede wall;
                    wall.setHeight(WALL_HEIGHT);
                    parede_array.push_back(wall);
                    Coord v1;
                    v1.x = i;
                    v1.y = 0;
                    v1.z = j;
                    parede_array.back().setStartPoint(v1);
                    inWall = true;
                }
                Coord v1;
                v1.x = i;
                v1.y = 0;
                v1.z = j+1;
                parede_array.back().setEndPoint(v1);

            }
            else if(map[i][j]==1 && map[i][j+1]==0 ){
                inWall = false;
            }
        }
    }

    inWall = false;
    for(int j=0;j<tamanho;j++){
        for(int i=0;i<tamanho;i++){
            if(map[i][j]==1 && map[i+1][j]==1 ){
                if(!inWall){
                    Parede wall;
                    wall.setHeight(WALL_HEIGHT);
                    parede_array.push_back(wall);
                    Coord v1;
                    v1.x = i;
                    v1.y = 0;
                    v1.z = j;
                    parede_array.back().setStartPoint(v1);
                    inWall = true;
                }
                Coord v1;
                v1.x = i+1;
                v1.y = 0;
                v1.z = j;
                parede_array.back().setEndPoint(v1);
                
            }
            else if(map[i][j]==1 && map[i+1][j]==0 ){
                inWall = false; 
            }
        }
    }
}


void initAlvos(){
    Alvo alvo1;
    Coord pos, direcao;
    pos.x=13.9;
    pos.y=1;
    pos.z=10;
    alvo1.setPos(pos);
    direcao.x=1;
    direcao.y=0;
    direcao.z=0;
    alvo1.setDirecao(direcao);
    alvo_array.push_back(alvo1);


    Alvo alvo2;
    Coord pos2, direcao2;
    pos2.x=13.9;
    pos2.y=1;
    pos2.z=5;
    alvo2.setPos(pos2);
    direcao2.x=1;
    direcao2.y=0;
    direcao2.z=0;
    alvo2.setDirecao(direcao2);
    alvo_array.push_back(alvo2);


    Alvo alvo3;
    Coord pos3, direcao3;
    pos3.x=7;
    pos3.y=1;
    pos3.z=37.9;
    alvo3.setPos(pos3);
    direcao3.x=0;
    direcao3.y=0;
    direcao3.z=-1;
    alvo3.setDirecao(direcao3);
    alvo_array.push_back(alvo3);

    Alvo alvo4;
    Coord pos4, direcao4;
    pos4.x=0.01;
    pos4.y=1;
    pos4.z=31;
    alvo4.setPos(pos4);
    direcao4.x=-1;
    direcao4.y=0;
    direcao4.z=0;
    alvo4.setDirecao(direcao4);
    alvo_array.push_back(alvo4);


    Alvo alvo5;
    Coord pos5, direcao5;
    pos5.x=27;
    pos5.y=1;
    pos5.z=38;
    alvo5.setPos(pos5);
    direcao5.x=0;
    direcao5.y=0;
    direcao5.z=-1;
    alvo5.setDirecao(direcao5);
    alvo_array.push_back(alvo5);

    Alvo alvo6;
    Coord pos6, direcao6;
    pos6.x=26;
    pos6.y=1;
    pos6.z=10;
    alvo6.setPos(pos6);
    direcao6.x=0;
    direcao6.y=0;
    direcao6.z=-1;
    alvo6.setDirecao(direcao6);
    alvo_array.push_back(alvo6);

}



void criaDefineTexturas()
{
	//http://opengameart.org
	glGenTextures(1, &texture[0]);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	imag.LoadBmpFile("stone_wall02.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
	imag.GetNumCols(),
		imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		imag.ImageData());

    glGenTextures(1, &texture[1]);
    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    imag.LoadBmpFile("tileable_cobble_01_diffuse.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());
	
    glGenTextures(1, &texture[2]);
    glBindTexture(GL_TEXTURE_2D, texture[2]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    imag.LoadBmpFile("imgp5505_seamless.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());

    glGenTextures(1, &texture[3]);
    glBindTexture(GL_TEXTURE_2D, texture[3]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    imag.LoadBmpFile("Boards_S.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());
    
    glGenTextures(1, &texture[4]);
    glBindTexture(GL_TEXTURE_2D, texture[4]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    imag.LoadBmpFile("paving_2.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());

    glGenTextures(1, &texture[5]);
    glBindTexture(GL_TEXTURE_2D, texture[5]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-back.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());	
    
    glGenTextures(1, &texture[6]);
    glBindTexture(GL_TEXTURE_2D, texture[6]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-bottom.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData()); 

    glGenTextures(1, &texture[7]);
    glBindTexture(GL_TEXTURE_2D, texture[7]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-front.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());  

    glGenTextures(1, &texture[8]);
    glBindTexture(GL_TEXTURE_2D, texture[8]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-left.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());  

    glGenTextures(1, &texture[9]);
    glBindTexture(GL_TEXTURE_2D, texture[9]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-right.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());  

    glGenTextures(1, &texture[10]);
    glBindTexture(GL_TEXTURE_2D, texture[10]);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    imag.LoadBmpFile("bluesky1-top.bmp");
    glTexImage2D(GL_TEXTURE_2D, 0, 3, 
    imag.GetNumCols(),
        imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
        imag.ImageData());  
}

void init(void)
{   
    force = 1;
	glClearColor(WHITE);
	glShadeModel(GL_SMOOTH);
	criaDefineTexturas();

	criaDefineTexturas( );
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ); 
    

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glutSetCursor(GLUT_CURSOR_NONE);
	glutWarpPointer(g_viewport_width/2, g_viewport_height/2);
	
	initLights();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);
    glEnable(GL_LIGHT5);

    initNevoeiro();

    
	initMap();
    initAlvos();

	glEnable(GL_DEPTH_TEST);
}



void desenhaTexto(char *string, GLfloat x, GLfloat y, GLfloat z) 
{  
    glRasterPos3f(x,y,z); 
    while(*string)
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *string++); 
}


void drawScene(){

    glDisable(GL_LIGHTING);

    
    if(g_camera.GetPos().x>=0 && g_camera.GetPos().x<=15 && g_camera.GetPos().z>=23 && g_camera.GetPos().z<=38){
        inside = true;
    }
    else{
        inside = false;
    }

    if(lanterna){
        lanternaPosition[0]=g_camera.GetPos().x;
        lanternaPosition[1]=g_camera.GetPos().y;
        lanternaPosition[2]=g_camera.GetPos().z;
        lanternaDirection[0]=g_camera.GetDirectionVector().x;
        lanternaDirection[1]=g_camera.GetDirectionVector().y;
        lanternaDirection[2]=g_camera.GetDirectionVector().z;



        glLightfv(GL_LIGHT4,GL_POSITION,                lanternaPosition);
        glLightfv(GL_LIGHT4,GL_DIFFUSE,                 lanternaColor);
        glLightf(GL_LIGHT4,GL_CONSTANT_ATTENUATION,     1.0);
        glLightf(GL_LIGHT4,GL_LINEAR_ATTENUATION,       0.05);
        glLightf(GL_LIGHT4,GL_QUADRATIC_ATTENUATION,    0.0);
        glLightf(GL_LIGHT4,GL_SPOT_CUTOFF,              60.0);
        glLightfv(GL_LIGHT4,GL_SPOT_DIRECTION,          lanternaDirection);
        glLightf(GL_LIGHT4,GL_SPOT_EXPONENT,            4.0);
        glEnable(GL_LIGHT4);
    }else{
        glDisable(GL_LIGHT4);
    }
    initLights();
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);


    glPushMatrix(); 
        drawSkybox();

        drawCandieiros();
    glPopMatrix();

    // ---------------------------- chao
    glPushMatrix();
        int fim_x = g_camera.GetPos().x+20;
        int fim_z = g_camera.GetPos().z+20;
        for(int i = (int)g_camera.GetPos().x-20; i < fim_x; i++) {
            for(int j = (int)g_camera.GetPos().z-20;j<fim_z;j++){
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D,texture[4]);
                glBegin(GL_QUADS);
                    glColor4f(WHITE);
                    glTexCoord2f(0.0, 0.0);
                    glNormal3f(0,1,0);
                    glVertex3f(i, 0.0, j);
                    glTexCoord2f(1.0, 0.0);
                    glNormal3f(0,1,0);
                    glVertex3f(i+1, 0.0, j);
                    glTexCoord2f(1.0, 1.0);
                    glNormal3f(0,1,0);
                    glVertex3f(i+1, 0.0, j+1);
                    glTexCoord2f(0.0, 1.0);
                    glNormal3f(0,1,0);
                    glVertex3f(i, 0.0, j+1);        
                glEnd();
                glDisable(GL_TEXTURE_2D);
            }
        }
    glPopMatrix();   

	//------------------------------parede
	drawWalls();


    //---------------------setas
    drawArrows();


    //--------------------alvos
    moveAlvo();
    drawAlvos();


    //---------------------------setas paradas
    drawStaticArrows();

    //vidro
    drawGlass();


    
    glDisable(GL_DEPTH_TEST);
    glLoadIdentity();
    glColor4f(VERDE);
    sprintf(texto, "Pontuacao : %d", pontuacao_jogador);
    desenhaTexto(texto,-15,8,-9);
    sprintf(texto, "Numero de setas disparadas : %d\n", setas_disparadas);
    desenhaTexto(texto,-15,7.5,-9);
    sprintf(texto, "Forca : %f ", force);
    desenhaTexto(texto,-15,7,-9);
    glEnable(GL_DEPTH_TEST);

   //crosshair
	glDisable(GL_DEPTH_TEST);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-35.0f);   

	glPushMatrix();
		glColor4f(VERDE);
		glBegin(GL_LINES);
			glVertex3f(0,1,0);
			glVertex3f(0,-1,0);
			glVertex3f(-1,0,0);
			glVertex3f(1,0,0);
		glEnd();
    glPopMatrix();


	glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
	glutPostRedisplay();
}

void moveAlvo(){
    Coord aux = alvo_array[4].getPos();
    if(goingUp && aux.x >= 31){
        aux.x-=0.1;
        goingUp = false;
    }else if(!goingUp && aux.x <=20){
        aux.x+=0.1;
        goingUp = true;
    }
    else if(goingUp){
        aux.x+=0.1;
    }
    else if(!goingUp){
        aux.x-=0.1;
    }
    alvo_array[4].setPos(aux);
}

void drawCandieiros(){

    glPushMatrix();
        glColor3f(1.0f,0.0f,0.0f);
        glTranslatef(0.0,3.0,23.0);          
        glutSolidSphere(0.1f, 100, 100);
    glPopMatrix();
    glPushMatrix();
        glColor3f(0.0f,1.0f,0.0f);
        glTranslatef(0.0,3.0,38.0);          
        glutSolidSphere(0.1f, 100, 100);
    glPopMatrix();
    glPushMatrix();
        glColor3f(1.0f,1.0f,0.0f);
        glTranslatef(14.0,3.0,38.0);          
        glutSolidSphere(0.1f, 100, 100);
    glPopMatrix();
    glPushMatrix();
        glColor3f(0.0f,0.0f,1.0f);
        glTranslatef(14.0,3.0,23.0);          
        glutSolidSphere(0.1f, 100, 100);
    glPopMatrix();

}

//http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=13
void drawSkybox(){
    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[5]);//back
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(-75.0,-75.0 , 75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(-75.0, -75.0, -75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(-75.0, 75.0, -75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(-75.0, 75.0, 75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[6]);//down
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(-75.0,-75.0 , -75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(-75.0, -75.0, 75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(75.0, -75.0, 75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(75.0, -75.0, -75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[7]);//front
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(75.0,-75.0 , -75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(75.0, -75.0, 75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(75.0, 75.0, 75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(75.0, 75.0, -75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[8]); //left
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(-75.0,-75.0 , -75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(75.0, -75.0, -75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(75.0, 75.0, -75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(-75.0, 75.0, -75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[9]); //right
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(75.0,-75.0 , 75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(-75.0, -75.0, 75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(-75.0, 75.0, 75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(75.0, 75.0, 75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,texture[10]); //up
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glVertex3f(75.0, 75.0 , -75.0);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(75.0, 75.0, 75.0);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(-75.0, 75.0, 75.0);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(-75.0, 75.0, -75.0);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}


void drawWalls(){
    //tecto
    for(int i = 0; i < 14; i++){
        for(int j = 23; j < 38; j++){
            glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D,texture[3]);
                    glBegin(GL_QUADS);
                        glNormal3f(0,-1,0);
                        glColor4f(WHITE);
                        glTexCoord2f(0.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j);
                        glTexCoord2f(1.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j+1);
                        glTexCoord2f(1.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j+1);
                        glTexCoord2f(0.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j);      
                    glEnd();
                    glDisable(GL_TEXTURE_2D);
            glPopMatrix();
        }
    }
//tecto
    for(int i = 14; i < 18; i++){
        for(int j = 30; j < 32; j++){
            glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D,texture[3]);
                    glBegin(GL_QUADS);
                        glColor4f(WHITE);
                        glTexCoord2f(0.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j);
                        glTexCoord2f(1.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j+1);
                        glTexCoord2f(1.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j+1);
                        glTexCoord2f(0.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j);      
                    glEnd();
                    glDisable(GL_TEXTURE_2D);
            glPopMatrix();
        }
    }
//tecto
    for(int i = 18; i < 32; i++){
        for(int j = 23; j < 44; j++){
            glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D,texture[3]);
                    glBegin(GL_QUADS);
                        glColor4f(WHITE);
                        glTexCoord2f(0.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j);
                        glTexCoord2f(1.0, 0.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i, 3.0, j+1);
                        glTexCoord2f(1.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j+1);
                        glTexCoord2f(0.0, 1.0);
                        glNormal3f(0,-1,0);
                        glVertex3f(i+1, 3.0, j);      
                    glEnd();
                    glDisable(GL_TEXTURE_2D);
            glPopMatrix();
        }
    }

    //parede curta
    for(int i=18;i<32;i++){
        int j = 35;
        glPushMatrix();
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D,texture[2]);
            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glTexCoord2f(0.0, 0.0);
                glNormal3f(0,0,-1);
                glVertex3f(i, 0.0, j);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(i+1, 0.0, j);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(i+1, 0.5, j);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(i, 0.5, j);      
            glEnd();

            glBegin(GL_QUADS);
                glColor4f(WHITE);
                glNormal3f(0,1,0);
                glTexCoord2f(0.0, 0.0);
                glNormal3f(0,0,-1);
                glVertex3f(i, 0.5, j);
                glTexCoord2f(1.0, 0.0);
                glVertex3f(i, 0.5, j+1);
                glTexCoord2f(1.0, 1.0);
                glVertex3f(i+1, 0.5, j+1);
                glTexCoord2f(0.0, 1.0);
                glVertex3f(i+1, 0.5, j);      
            glEnd();
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();   
    }


    vector<Parede>::iterator v = parede_array.begin();
    while(v != parede_array.end()){
        GLfloat height = (*v).getHeight();
        //parede vertical
        if((*v).getStartPoint().x != (*v).getEndPoint().x){
            int i = (int)(*v).getStartPoint().x;
            int fim = (int)(*v).getEndPoint().x;
            int z = (int)(*v).getStartPoint().z;
            for(;i<fim;i++){
                for(int l = 0; l<height;l++){
                glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D,texture[0]);
                    glBegin(GL_QUADS);
                        if(z == 4 || z==3 || z==23 || z==18){
                            glNormal3f(0,0,1);
                        }
                        else if(z == 15 || z==19 || z==21 || z==38 || z==34){
                            glNormal3f(0,0,-1);
                        }
                        glColor4f(WHITE);
                        glTexCoord2f(0.0, 0.0);
                        glVertex3f(i, l, z);
                        glTexCoord2f(1.0, 0.0);
                        glVertex3f(i+1, l, z);
                        glTexCoord2f(1.0, 1.0);
                        glVertex3f(i+1, l+1, z);
                        glTexCoord2f(0.0, 1.0);
                        glVertex3f(i, l+1, z);      
                    glEnd();
                    glDisable(GL_TEXTURE_2D);
                glPopMatrix();
                }

            }
        }
        //parede horizontal
        else if((*v).getStartPoint().z != (*v).getEndPoint().z){
            int i = (int)(*v).getStartPoint().z;
            int fim = (int)(*v).getEndPoint().z;
            int x = (int)(*v).getStartPoint().x;
            for(;i<fim;i++){
                for(int l = 0; l<height;l++){
                glPushMatrix();
                    glEnable(GL_TEXTURE_2D);
                    glBindTexture(GL_TEXTURE_2D,texture[0]);
                    glBegin(GL_QUADS);
                        glColor4f(WHITE);
                        if(x == 0 || x==1 || x==5 || x==18 || x==29){
                            glNormal3f(1,0,0);
                        }
                        else if(x == 14 || x==4 || x==8 || x==32 || x==31){
                            glNormal3f(-1,0,0);
                        }

                        glTexCoord2f(0.0, 0.0);
                        glVertex3f(x, l, i);
                        glTexCoord2f(1.0, 0.0);
                        glVertex3f(x, l, i+1);
                        glTexCoord2f(1.0, 1.0);
                        glVertex3f(x, l+1, i+1);
                        glTexCoord2f(0.0, 1.0);
                        glVertex3f(x, l+1, i);      
                    glEnd();
                    glDisable(GL_TEXTURE_2D);
                glPopMatrix();
                }
            }
        }
        v++;
    }
}

void drawAlvos(){
    for(GLint i = 0; i < (GLint) alvo_array.size(); i++){
        glPushMatrix();
            //Translate do alvo
            glTranslatef(alvo_array[i].getPos().x, alvo_array[i].getPos().y, alvo_array[i].getPos().z);
            //Rodar a seta      
            /*Say you want to rotate vector A to vector B. In your case vector A would be the Z axis (0,0,1).
            Let vector C = A x B (cross product routine). Next get the angle (theta) between A and B.
            The formula for this is: theta = acos(dot(A,B)), where 'dot' means dot product, and acos means arccos.
            Finally, call glRotate with the angle theta and vector C just before the gluCylinder call. In case you forgot, the vector C is normal to the plane containing A and B.
            So you're rotating A around C until it lines up with B*/
            Coord z;
            z.x=0; z.y=0; z.z=1;
            Coord c = crossProduct(z,alvo_array[i].getDirecao());
            GLfloat angulo = angle(alvo_array[i].getDirecao(),z) * 180 / M_PI;
            glRotatef(180,0,1,0);
            glRotatef(angulo,c.x,c.y,c.z);
            //desenhar o alvo

            alvo_array[i].Init(); 
        glPopMatrix();    
    }

    glPushMatrix();
        glTranslatef(alvo_array[5].getPos().x, alvo_array[5].getPos().y, alvo_array[5].getPos().z);
        GLUquadricObj *quadratic;
        quadratic = gluNewQuadric();
        
        glEnable(GL_TEXTURE_2D);       
        glBindTexture(GL_TEXTURE_2D, texture[3]);
        gluQuadricTexture(quadratic,1);
        
        glPushMatrix();
            glColor4f(1,1,1,1);
            glTranslatef(0,0,-1.05);
            glRotatef(-45,0,0,-1);
            gluCylinder(quadratic,1,1,1,4,1);
        glPopMatrix();
        glPushMatrix();
            glRotatef(-45,0,0,-1);
            glTranslatef(0,0,-1.05);
            glColor4f(1,1,1,1);
            gluDisk(quadratic, 0.0f, 1, 4, 1);
            glTranslatef(0,0,1);
            glColor4f(1,1,1,1);
            gluDisk(quadratic, 0.0f, 1, 4, 1);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0,0,-0.55);
            glTranslatef(0,1.35,0);
            gluSphere(quadratic,0.7,10,10);
        glPopMatrix();

        glDisable(GL_TEXTURE_2D);
    glPopMatrix(); 

}



void drawArrow(Coord cor1,Coord cor2,Coord cor3){
    glPushMatrix();
        glTranslatef(0.0,0.0,-0.25);
        GLUquadricObj *quadratic;
        quadratic = gluNewQuadric();
        glPushMatrix();
            glColor4f(cor1.x,cor1.y,cor1.z,1);
            gluCylinder(quadratic,0.02,0,0.15,4,1);
            glRotatef(180, 1,0,0);
            gluDisk(quadratic, 0.0f, 0.02, 4, 1);
        glPopMatrix();

        glTranslatef(0.0,0.0,-0.35);
        
        glPushMatrix();
            glColor4f(cor2.x,cor2.y,cor2.z,1);
            gluCylinder(quadratic,0.01,0.01,0.35,15,10);
            glRotatef(180, 1,0,0);
            gluDisk(quadratic, 0.0f, 0.01, 15, 1);
        glPopMatrix();
        
        glTranslatef(0.0,0.0,0.05);

        glPushMatrix();
            glTranslatef(0.0,0.005,0.0);
            glColor4f(cor3.x,cor3.y,cor3.z,1);
            glBegin(GL_TRIANGLES);
                glVertex3f(0,0,0);
                glVertex3f(0,0,-0.05);
                glVertex3f(0,0.05,-0.1);
            glEnd();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.0,-0.005,0.0);
            glRotatef(180,0,0,1);
            glColor4f(cor3.x,cor3.y,cor3.z,1);
            glBegin(GL_TRIANGLES);
                glVertex3f(0,0,0);
                glVertex3f(0,0,-0.05);
                glVertex3f(0,0.05,-0.1);
            glEnd();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.005,0.0,0.0);
            glRotatef(-90,0,0,1);
            glColor4f(cor3.x,cor3.y,cor3.z,1);
            glBegin(GL_TRIANGLES);
                glVertex3f(0,0,0);
                glVertex3f(0,0,-0.05);
                glVertex3f(0,0.05,-0.1);
            glEnd();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-0.005,0.0,0.0);
            glRotatef(90,0,0,1);
            glColor4f(cor3.x,cor3.y,cor3.z,1);
            glBegin(GL_TRIANGLES);
                glVertex3f(0,0,0);
                glVertex3f(0,0,-0.05);
                glVertex3f(0,0.05,-0.1);
            glEnd();
        glPopMatrix();

        gluDeleteQuadric(quadratic);
    glPopMatrix();
}

void drawArrows(void){
    //atualizar a posicao das setas. o vetor y vai sendo diminuido https://en.wikipedia.org/wiki/Trajectory_of_a_projectile
    for(GLint i = 0; i < (GLint)arrow_array.size(); i++){
	glPushMatrix();
        arrow_array[i].pos.x = arrow_array[i].pos.x + arrow_array[i].vectores.x*0.1;
        arrow_array[i].pos.y = arrow_array[i].pos.y + arrow_array[i].vectores.y*0.1;
        arrow_array[i].pos.z = arrow_array[i].pos.z + arrow_array[i].vectores.z*0.1;

        arrow_array[i].vectores.y = (GLfloat)arrow_array[i].speed * sin(arrow_array[i].angle) - 9.8*arrow_array[i].time/(arrow_array[i].speed*cos(arrow_array[i].angle));
        arrow_array[i].time += 0.005;
        //Translate da setas
        glTranslatef(arrow_array[i].pos.x, arrow_array[i].pos.y, arrow_array[i].pos.z);
        //Rodar a seta      
        /*Say you want to rotate vector A to vector B. In your case vector A would be the Z axis (0,0,1).
        Let vector C = A x B (cross product routine). Next get the angle (theta) between A and B.
        The formula for this is: theta = acos(dot(A,B)), where 'dot' means dot product, and acos means arccos.
        Finally, call glRotate with the angle theta and vector C just before the gluCylinder call. In case you forgot, the vector C is normal to the plane containing A and B.
        So you're rotating A around C until it lines up with B*/
        Coord z;
        z.x=0; z.y=0; z.z=1;
        Coord c = crossProduct(z,arrow_array[i].vectores);
        GLfloat angulo = angle(arrow_array[i].vectores,z) * 180 / M_PI;
        glRotatef(angulo,c.x,c.y,c.z);
        //desenhar a seta
        drawArrow( arrow_array[i].cor1, arrow_array[i].cor2, arrow_array[i].cor3);

    glPopMatrix();     
    }
}


void drawStaticArrows(void){
    for(GLint i = 0; i < (GLint)static_arrow_array.size(); i++){
    glPushMatrix();
        //Translate da setas
        glTranslatef(static_arrow_array[i].pos.x, static_arrow_array[i].pos.y, static_arrow_array[i].pos.z);
        //Rodar a seta      
        /*Say you want to rotate vector A to vector B. In your case vector A would be the Z axis (0,0,1).
        Let vector C = A x B (cross product routine). Next get the angle (theta) between A and B.
        The formula for this is: theta = acos(dot(A,B)), where 'dot' means dot product, and acos means arccos.
        Finally, call glRotate with the angle theta and vector C just before the gluCylinder call. In case you forgot, the vector C is normal to the plane containing A and B.
        So you're rotating A around C until it lines up with B*/
        Coord z;
        z.x=0; z.y=0; z.z=1;
        Coord c = crossProduct(z,static_arrow_array[i].vectores);
        GLfloat angulo = angle(static_arrow_array[i].vectores,z) * 180 / M_PI;
        glRotatef(angulo,c.x,c.y,c.z);
        //desenhar a seta

        drawArrow(static_arrow_array[i].cor1,static_arrow_array[i].cor2,static_arrow_array[i].cor3);

    glPopMatrix();     
    }
}

void drawGlass(){
    glColor4f(0,0.1,0.6,0.2);
    glBegin(GL_QUADS);
        glVertex3f(7, 0.0, 0);
        glVertex3f(7, 0.0, 15);
        glVertex3f(7, 2, 15);
        glVertex3f(7, 2, 0);        
    glEnd();

}



void display(void){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//================================================================= Viewport1
	glViewport (0, 0, (GLsizei)g_viewport_width, (GLsizei)g_viewport_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0, g_viewport_width/(GLfloat)g_viewport_height, 0.1, 200.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	

	g_camera.Refresh();


    CheckArrowColisions();


    drawScene();  


	//glFlush ();
	glutSwapBuffers();
}


//======================================================= EVENTOS

//Aumentar a forca a medida que temos o rato premido
void onClickDown(void){
    if(force <=3){
        force += 0.02;
    }
    glutPostRedisplay();
}

void shootArrow(void){
    //caso o utilizador libertar o rato, criar a seta e headshot


    if(force != 1){
        Arrow new_arrow;

        //SETA TEM A POSIÇAO E VETORES INICIAIS IGUAIS A CAMERA
        new_arrow.pos = g_camera.GetPos();
        new_arrow.vectores = g_camera.GetDirectionVector();
        new_arrow.vectores.x *= force;
        new_arrow.vectores.y *= force;
        new_arrow.vectores.z *= force;
        new_arrow.time=0;
        new_arrow.speed = force;

        srand (time(NULL));

        Coord cor1,cor2,cor3;

        cor1.x = ((GLfloat) rand() / (RAND_MAX));
        cor1.y = ((GLfloat) rand() / (RAND_MAX));
        cor1.z = ((GLfloat) rand() / (RAND_MAX));
        cor2.x = ((GLfloat) rand() / (RAND_MAX));
        cor2.y = ((GLfloat) rand() / (RAND_MAX));
        cor2.z = ((GLfloat) rand() / (RAND_MAX));
        cor3.x = ((GLfloat) rand() / (RAND_MAX));
        cor3.y = ((GLfloat) rand() / (RAND_MAX));
        cor3.z = ((GLfloat) rand() / (RAND_MAX));

        new_arrow.cor1 = cor1;
        new_arrow.cor2 = cor2;
        new_arrow.cor3 = cor3;


        Coord u;
        u.x = new_arrow.vectores.x;
        u.y = 0;
        u.z = new_arrow.vectores.z;
        //DEFINIR O ANGULO ENTRE A O DISPARO E O "CHAO"
        new_arrow.angle = angle(u,new_arrow.vectores);
        //printf("Angulo: %f\n", new_arrow.angle*180/M_PI);


        //ADICIONAR A ARROW AO ARRAY
        arrow_array.push_back(new_arrow);

        setas_disparadas++;
        //REINICIAR A FORÇA
        force = 1;  
    }
}


void CheckArrowColisions(void){
    vector<Arrow>::iterator v = arrow_array.begin();
    while( v != arrow_array.end()) {
        //Ground colision
        if((*v).pos.y <= 0){
            Arrow aux = (*v);
            static_arrow_array.push_back(aux);
            arrow_array.erase(v);
        }//Alvo colision
        else if(CheckAlvoArrowColision(v)){
            Arrow aux = (*v);
            static_arrow_array.push_back(aux);
            arrow_array.erase(v);
        }
        //Wall Colision
        else if(CheckArrowWallColisions(v)){
            Arrow aux = (*v);
            static_arrow_array.push_back(aux);
            arrow_array.erase(v);
        }
        else{
            v++;
        }
    }
}


bool CheckArrowWallColisions(vector<Arrow>::iterator v){
    Coord a1;
    a1.x=7; a1.y=(*v).pos.y; a1.z=0;
    Coord a2;
    a2.x=7; a2.y=(*v).pos.y; a2.z=15;
    Coord arrows1 = (*v).pos;
    if(arrows1.y <= 2.0){
        GLfloat dist = (GLfloat)sqrt((a2.x-a1.x)*(a2.x-a1.x)+(a2.y-a1.y)*(a2.y-a1.y)+(a2.z-a1.z)*(a2.z-a1.z));
        GLfloat dist2 = (GLfloat)sqrt((arrows1.x-a1.x)*(arrows1.x-a1.x)+(arrows1.y-a1.y)*(arrows1.y-a1.y)+(arrows1.z-a1.z)*(arrows1.z-a1.z));
        GLfloat dist3 = (GLfloat)sqrt((arrows1.x-a2.x)*(arrows1.x-a2.x)+(arrows1.y-a2.y)*(arrows1.y-a2.y)+(arrows1.z-a2.z)*(arrows1.z-a2.z));

        
        if(dist2+dist3 <= dist + 0.005) {
            //printf("colisao\n");
            return true;
        }
    }

    vector<Parede>::iterator p = parede_array.begin();
    while( p != parede_array.end()){
        Coord p1 = (*p).getStartPoint();
        p1.y = (*v).pos.y;
        Coord p2 = (*p).getEndPoint();
        p2.y = (*v).pos.y;
        Coord arrows = (*v).pos;

        if(arrows.y <= (*p).getHeight()){
            GLfloat dist = (GLfloat)sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y)+(p2.z-p1.z)*(p2.z-p1.z));
            GLfloat dist2 = (GLfloat)sqrt((arrows.x-p1.x)*(arrows.x-p1.x)+(arrows.y-p1.y)*(arrows.y-p1.y)+(arrows.z-p1.z)*(arrows.z-p1.z));
            GLfloat dist3 = (GLfloat)sqrt((arrows.x-p2.x)*(arrows.x-p2.x)+(arrows.y-p2.y)*(arrows.y-p2.y)+(arrows.z-p2.z)*(arrows.z-p2.z));

            
            if(dist2+dist3 <= dist + 0.001) {
                //printf("colisao\n");
                return true;
            }
        }

        p++;
    }
    return false;
}


bool CheckAlvoArrowColision(vector<Arrow>::iterator v){
    vector<Alvo>::iterator p = alvo_array.begin();
    while(p != alvo_array.end()){
        Coord v2 = (*v).pos;
        Coord v1 = (*p).getPos();
        GLfloat dist = (GLfloat)sqrt((v2.x-v1.x)*(v2.x-v1.x)+(v2.y-v1.y)*(v2.y-v1.y)+(v2.z-v1.z)*(v2.z-v1.z));
        GLfloat deltaX = v2.x-v1.x;
        GLfloat deltaZ = v2.z-v1.z;
        bool horizontal = ((*p).getDirecao().x==1 || (*p).getDirecao().x==-1);
        if( dist <= 0.5 &&  (deltaX<=0.1 && deltaX>=-0.1) && horizontal ){
            if(dist <= 0.1){
                pontuacao_jogador+=50;
                return true;
            }
            else if(dist <= 0.2){
                pontuacao_jogador+=40;
                return true;
            }
            else if(dist <= 0.3){
                pontuacao_jogador+=30;
                return true;
            }
            else if(dist <= 0.4){
                pontuacao_jogador+=20;
                return true;
            }
            else if(dist <= 0.5){
                pontuacao_jogador+=10;
                return true;
            }

        }else if( dist <= 0.5 && !horizontal && (deltaZ<=0.1 && deltaZ>=-0.1)  ){
            if(dist <= 0.1){
                pontuacao_jogador+=50;
                return true;
            }
            else if(dist <= 0.2){
                pontuacao_jogador+=40;
                return true;
            }
            else if(dist <= 0.3){
                pontuacao_jogador+=30;
                return true;
            }
            else if(dist <= 0.4){
                pontuacao_jogador+=20;
                return true;
            }
            else if(dist <= 0.5){
                pontuacao_jogador+=10;
                return true;
            }

        }
        p++;
    }
    return false;
}

void Reshape (int w, int h) {
    g_viewport_width = w;
    g_viewport_height = h;

    glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
    glMatrixMode (GL_PROJECTION); //set the matrix to projection

    glLoadIdentity ();
    gluPerspective (90.0, (GLfloat)w / (GLfloat)h, 0.1 , 100.0); //set the perspective (angle of sight, width, height, ,depth)
    glMatrixMode (GL_MODELVIEW); //set the matrix back to model
}


void moveCamera(void){
	if(g_key[32])
        g_translation_speed = 0.15;
    if(!g_key[32])
        g_translation_speed = 0.05;
    if(g_key['w'] || g_key['W']) {
        g_camera.Move(g_translation_speed);
    }
    if(g_key['s'] || g_key['S']) {
        g_camera.Move(-g_translation_speed);
    }
    if(g_key['a'] || g_key['A']) {
        g_camera.Strafe(g_translation_speed);
    }
    if(g_key['d'] || g_key['D']) {
        g_camera.Strafe(-g_translation_speed);
    }
    if(g_mouse_left_down){
    	glutIdleFunc(onClickDown);
    }
    if(!g_mouse_left_down){
    	glutIdleFunc(shootArrow);
    }

}


void Update(int value){
	moveCamera();
	glutPostRedisplay();
    glutTimerFunc(30, Update, 0);
}

void UpdateLights(int value){
    srand (time(NULL));
    GLfloat r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection[0]=r;
    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection[2]=r;

    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection2[0]=r;
    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection2[2]=-r;

    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection3[0]=-r;
    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection3[2]=+r;

    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection4[0]=-r;
    r = ((GLfloat) rand() / (RAND_MAX));
    spotlightDirection4[2]=-r;
    glutTimerFunc(1000, UpdateLights, 0);
}

void teclado (unsigned char key, int x, int y){
    if (key==27)
    {
    exit(0);
    }
    g_key[key] = true;
}

void Mouse(int button, int state, int x, int y){

    if(state == GLUT_DOWN) {
        if(button == GLUT_LEFT_BUTTON) {
            g_mouse_left_down = true;
        }
        else if(button == GLUT_RIGHT_BUTTON) {
            g_mouse_right_down = true;
        }
    }
    else if(state == GLUT_UP) {
        if(button == GLUT_LEFT_BUTTON) {
            g_mouse_left_down = false;
        }
        else if(button == GLUT_RIGHT_BUTTON) {
            g_mouse_right_down = false;
        }
    }
}


void KeyboardUp(unsigned char key, int x, int y){
    if(g_key['f'] || g_key['F']) {
        fog=!fog;
        if(fog)
            glEnable(GL_FOG);
        else
            glDisable(GL_FOG);
    }
    if(g_key['l'] || g_key['L']) {
        lanterna=!lanterna;
    }
    if(g_key['n'] || g_key['N']) {
        light=!light;
        printf("%d\n", light);
    }

    g_key[key] = false;

}

/*source http://nghiaho.com/?p=1613*/
void MouseMotion(int x, int y){
    // This variable is hack to stop glutWarpPointer from triggering an event callback to Mouse(...)
    // This avoids it being called recursively and hanging up the event loop
    static bool just_warped = false;

    if(just_warped) {
        just_warped = false;
        return;
    }

    int dx = x - g_viewport_width/2;
    int dy = y - g_viewport_height/2;

    if(dx) {
        g_camera.RotateYaw(g_rotation_speed*dx);
    }

    if(dy) {
        g_camera.RotatePitch(-g_rotation_speed*dy);
    }

    glutWarpPointer(g_viewport_width/2, g_viewport_height/2);

    just_warped = true;
    
}

//======================================================= MAIN
int main(int argc, char** argv){

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitWindowSize (g_viewport_width, g_viewport_height); 
	glutInitWindowPosition (400, 100); 
  	glutCreateWindow ("my awesome cg project");

	init();


	glutDisplayFunc(display);
	
	glutMouseFunc(Mouse);
    glutMotionFunc(MouseMotion);
    glutPassiveMotionFunc(MouseMotion); 
	glutKeyboardFunc(teclado);
    glutKeyboardUpFunc(KeyboardUp);

    glutTimerFunc(1, Update, 0);
    glutTimerFunc(1, UpdateLights, 0);
	glutMainLoop();

	return 0;
}


